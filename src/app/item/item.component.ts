
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ItemService } from '../item.service';
import { AngularFireDatabase } from '@angular/fire/database';

@Component({
  selector: 'item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.css']
})
export class ItemComponent implements OnInit {
  @Input() data:any; //חיב לפתוח כדי שיוכל להעביר נתונים לאבא ולהוסיף בנג'י און איניט את כל מה שרשום למטה לפי
  name='';
  price='';
  key='';
  tempName;
  tempPrice;
  showTheButton = false;
  showEditField;
  showOption=false;
  checkboxFlag: boolean;


deleteItem(){//מוחקת באמת את האייטם
  this.itemService.deleteItem(this.key);
}
showButton(){
  this.showTheButton = true; 
  }
  hideButton(){
    this.showTheButton = false;
  
  }
  showEdit(){
    this.showEditField = true;
    this.tempName = this.name;
    this.tempPrice = this.price;
    this.showTheButton =true;
  }
  deleteItemOption()//פתיחת שני כפתורים
{
  this.showOption=true;
  this.showTheButton = false;

}  
checkChange()
{
  this.itemService.updateStock(this.key,this.checkboxFlag);
}
  canceDeleteItem(){
    this.showOption=false;
  }
  constructor(public itemService: ItemService, private db: AngularFireDatabase) { }

  ngOnInit() {// לפי השדות שרשומים בפיירבייס (צד ימין)
    this.name = this.data.name;
    this.price = this.data.price;
    this.checkboxFlag = this.data.stock;
    this.key = this.data.$key;
  }

}
