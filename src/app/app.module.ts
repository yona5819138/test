import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FormsModule} from '@angular/forms';

import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFireAuthModule } from '@angular/fire/auth';
import {environment} from '../environments/environment';


import { AppComponent } from './app.component';
import { NavComponent } from './nav/nav.component';
import { ItemsComponent } from './items/items.component';
import { ItemComponent } from './item/item.component';
import { LoginComponent } from './login/login.component';
import { NotFoundComponent } from './not-found/not-found.component';

import { Routes, RouterModule } from '@angular/router';
import {MatButtonModule,MatCheckboxModule,MatToolbarModule,MatInputModule,MatProgressSpinnerModule,MatCardModule,MatMenuModule, MatIconModule} from '@angular/material';
import {ReactiveFormsModule} from '@angular/forms';
import { GreetingsComponent } from './greetings/greetings.component';


@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    ItemsComponent,
    ItemComponent,
    LoginComponent,
    NotFoundComponent,
    GreetingsComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    AngularFireModule.initializeApp(environment.firebase),     
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    BrowserModule,
    MatButtonModule,
    BrowserAnimationsModule,
    MatInputModule,
    MatToolbarModule,
    MatCardModule,
    FormsModule,
    MatIconModule, 
    AngularFireModule.initializeApp(environment.firebase),    
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    ReactiveFormsModule,
    MatCheckboxModule,
      
    RouterModule.forRoot([
      {path:'',component:LoginComponent},
      {path:'login',component:LoginComponent},
      {path:'items',component:ItemsComponent},
      {path:'item',component:ItemComponent},
      {path:'not-found',component:NotFoundComponent},
      {path:'**',component:NotFoundComponent}

    ])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
