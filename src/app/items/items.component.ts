import { Component, OnInit } from '@angular/core';
import{AuthService} from '../auth.service';
import { Router } from '@angular/router';
import { AngularFireDatabase } from '@angular/fire/database';

@Component({
  selector: 'items',
  templateUrl: './items.component.html',
  styleUrls: ['./items.component.css']
})
export class ItemsComponent implements OnInit {
  items=[];
  item='';

  

constructor(private authService:AuthService,private router:Router,  private db: AngularFireDatabase, ) { }

  ngOnInit() {
    this.authService.user.subscribe(user => {   //פונים לסרביס של יוזר מסוים שהולך לדאטה בייס ומשם ממלא את טודוס במשימות שקיימות לו
      this.db.list('/users/' + user.uid + '/items').snapshotChanges().subscribe(//לבדוק בדאטהבייס איזה נתיב נכון
        items => {
          this.items = [];
          items.forEach(
            item => {
              let y = item.payload.toJSON();
              y["$key"] = item.key;
              this.items.push(y);//
            }
          )
        }
      )
    })
 
  }

  }