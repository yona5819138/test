import { Component, OnInit } from '@angular/core';
import{AuthService} from '../auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'greetings',
  templateUrl: './greetings.component.html',
  styleUrls: ['./greetings.component.css']
})
export class GreetingsComponent implements OnInit {
  email:string;

  constructor(public authService:AuthService) { }

  ngOnInit() {
    this.authService.user.subscribe(user => {
      console.log(user.email);
      if(user.email!=null)
       this.email=user.email;
     
   });
  }

}
