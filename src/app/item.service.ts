import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';
import { AngularFireDatabase } from '@angular/fire/database';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Injectable({
  providedIn: 'root'
})

export class ItemService {
  @Input() data:any;

  constructor(private authService: AuthService, private db: AngularFireDatabase) { }
  addItem(name:string, price:boolean)
  {
    this.authService.user.subscribe(user => {
      this.db.list('/users/'+user.uid+'/todos').push({'name':name, 'price':price});//מוסיף טודו לכל יוזר מסוים
    })
  }
  updateStock(key:string, stock:boolean)
  {
    this.authService.user.subscribe(user =>{
      this.db.list('/users/'+user.uid+'/items').update(key,{'stock':stock});
    })
    
  }
  deleteItem(key:string)
  {
    this.authService.user.subscribe(user => {
      this.db.list('/users/'+user.uid+'/items').remove(key);
    })
  }  
}
